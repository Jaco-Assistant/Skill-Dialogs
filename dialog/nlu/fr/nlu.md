## lookup:numbers2hundred
numbers2hundred.txt

## lookup:numbers2thousand
numbers2thousand.txt

## lookup:numbers_with_digits
numbers_with_digits.txt

## lookup:date
date.txt

## intent:abort
- Stop
- Interrompre
- Renoncer
- Abandonner
- Arrêter ça
- Annuler
- Quitter

## intent:refuse
- pourquoi je
- ne
- non
- non merci
- j'aimerais ne plus
- pas
- non non
- jamais
- certainement pas
- jamais de la vie
- sans moi
- ce sera sans moi
 
## intent:confirm
- super
- faisons ça
- ok
- bien sûr
- volontiers
- oui
- oui oui
- très volontiers
- oui, volontiers
- oui, avec plaisir
- très certainement
