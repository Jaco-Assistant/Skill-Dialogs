## lookup:numbers2hundred
numbers2hundred.txt

## lookup:numbers2thousand
numbers2thousand.txt

## lookup:numbers_with_digits
numbers_with_digits.txt

## lookup:date
date.txt

## intent:abort
- Stop
- Hör auf
- Abbrechen
- Beenden

## intent:refuse
- wieso sollte ich
- kein bock
- keine lust
- ne
- nein
- nein danke
- ne ich mag nicht mehr
- nicht
- nein nein
- nein, auf keinen fall
- niemals
 
## intent:confirm
- passt
- super
- machen wir
- ok
- natuerlich
- gerne
- ja
- ja ja
- sehr gerne
- ja gerne
- jawohl
- supidupi
