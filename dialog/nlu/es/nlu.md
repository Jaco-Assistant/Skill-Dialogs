## lookup:numbers2hundred
numbers2hundred.txt

## lookup:numbers2thousand
numbers2thousand.txt

## lookup:numbers_with_digits
numbers_with_digits.txt

## lookup:date
date.txt

## intent:abort
- Deténgase
- Basta ya.
- Cancelar
- Salga de

## intent:refuse
- ¿por qué iba a
- no hay dinero
- no hay lujuria
- ne
- No, gracias.
- Ya no me gusta.
- no
- no no
- nunca
 
## intent:confirm
- encaja
- super
- lo hacemos
- ok
- Por supuesto.
- con gusto
- sí
- Sí, sí.
- con mucho gusto
- Sí, con mucho gusto.
