# Try to list all examples for dates here, that the SLU-service can understand them
the day before yesterday
yesterday
today
tomorrow
the day after tomorrow
(last|this|next| ) (Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday)
January
February
March
April
May
June
July
August
September
October
November
December
Christmas
New Year's Eve
New Year
(last|this|next| ) weekend
(last|this|next) week
in (two|three|four|five|six|seven|ten|fourteen) days
in (two|three|four) weeks
