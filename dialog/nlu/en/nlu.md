## lookup:numbers2hundred
numbers2hundred.txt

## lookup:numbers2thousand
numbers2thousand.txt

## lookup:numbers_with_digits
numbers_with_digits.txt

## lookup:date
date.txt

## intent:abort
- Stop
- Abort
- Cancel
- Quit

## intent:refuse
- why would I
- no
- no thanks
- I do not like anymore
- not
- no no

## intent:confirm
- super
- we do
- ok
- of course
- gladly
- yes
- yes yes
