import os

import num2words as n2w

# ==================================================================================================


filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
dialogpath = filepath + "dialog/nlu/{}/"
path_hundred = dialogpath + "numbers2hundred.txt"
path_thousand = dialogpath + "numbers2thousand.txt"
path_digits = dialogpath + "numbers_with_digits.txt"
languages = ["de", "en", "es", "fr"]


# ==================================================================================================


def save_numbers(numbers: list, path: str):
    with open(path, "w+", encoding="utf-8") as file:
        file.write("\n".join(numbers) + "\n")


# ==================================================================================================


def main():

    for lang in languages:

        dirpath = dialogpath.format(lang)
        if not os.path.isdir(dirpath):
            os.makedirs(dirpath)

        # Numbers to hundred
        numbers = list(range(101))
        numbers = [n2w.num2words(n, lang=lang) for n in numbers]
        path = path_hundred.format(lang)
        save_numbers(numbers, path)

        # Numbers to thousand
        numbers = list(range(1001))
        numbers = [n2w.num2words(n, lang=lang) for n in numbers]
        path = path_thousand.format(lang)
        save_numbers(numbers, path)

        # Numbers with digits
        numbers = list(range(-1000, 1001))
        numbers = [n / 10.0 for n in numbers]
        numbers = [n2w.num2words(n, lang=lang) for n in numbers]
        path = path_digits.format(lang)
        save_numbers(numbers, path)


# ==================================================================================================

if __name__ == "__main__":
    main()
